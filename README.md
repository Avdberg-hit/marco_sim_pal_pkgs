# README #

These are the supporting packages by PAL robotics that are required to make the controllers & sensors etc work.

PAL makes use of their own forks of standard ROS package stacks:  
- marco_gazebo: makes use of the gazebo_ros package. This package is part of the **gazebo_ros_pkgs** fork by PAL.  
- marco_controller_configuration_gazebo:  
    - makes use of *joint_state_controller* & *force_torque_sensor_controller*, which are part of the **ros_controllers** fork by PAL  
    - makes use of *controller_manager*, which is part of the **ros_control** fork by PAL  

The hardware controllers used by the robot_descriptions are of type *pal_hardware_gazebo/PalHardwareGazebo*  
- This class is part of the **pal_hardware_gazebo**  
    - Which depends on **pal_hardware_interfaces**  
    - Which depends on **dynamic_introspection**  
    - Which depends on **ddynamic_reconfigure_python**, **pal_statistics**, **backwards_ros**  

Additionally:
- marco_bringup: uses **tf_lookup**  
- marco_controller_configuration_gazebo: uses **head_action**  

With these packages in the same workspace as the [marco_description_packages](https://bitbucket.org/Avdberg-hit/marco_description_packages/src/master/) you can set up a marco simulation & control it.